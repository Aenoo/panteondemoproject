﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RankTextPosition : MonoBehaviour
{
    public GameObject _gameManager;
    private GameObject[] _rankTextslist;

    private Vector3[] _textPos;
    public int myRank;




    void Start()
    {
        CreateRankTextList();


        // ***** find the position of texts

        _textPos = new Vector3[_rankTextslist.Length];
       
        for (int i = 0; i < _rankTextslist.Length; i++)
        {

            _textPos[i] = _rankTextslist[i].transform.position;

        }

    }

    // Update is called once per frame
    void Update()
    {

        // Sort the position of text according to the rank

        transform.position = Vector3.Lerp(transform.position, _textPos[myRank - 1], 0.3f);



    }

    // add all texts in board to ranktextlist 
    private void CreateRankTextList()
    {
        _rankTextslist = GameObject.FindGameObjectsWithTag("RankText");
    }




}
