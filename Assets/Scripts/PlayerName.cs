﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerName : MonoBehaviour
{

    // get input for name from user
    public TMP_InputField _inputName;

    public string saveName;

    public static bool _gameStarted = true;

  


    public void Start()
    {
        // show the the named that saved when game started

        GetComponent<RandomNames>().myName = PlayerPrefs.GetString("name", "");
        _inputName.GetComponent<TMP_InputField>().text = GetComponent<RandomNames>().myName;
    }

    

    public void SetName()
    {
        saveName = _inputName.GetComponent<TMP_InputField>().text;
        PlayerPrefs.SetString("name", saveName);

        GetComponent<RandomNames>().myName = _inputName.GetComponent<TMP_InputField>().text;
    }

}