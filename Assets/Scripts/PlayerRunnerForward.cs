﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRunnerForward : MonoBehaviour
{
    [SerializeField]
    float forwardSpeed = 5f;

    [SerializeField]
    Animator Animator;

    Rigidbody _playerRigidbody;

   


    private void Start()
    {
        _playerRigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        //** when the game begin, it provides to run to forwward on the z axis

        if (FixedTouchArea.firstPress&& FinishLineTrigger.touchingFinishline == false && GirlAgentScript._botTouchFinish == false)
        {
            if (GameManager._died == false)
            {
                transform.Translate(0, 0, forwardSpeed * Time.deltaTime, Space.Self);            
                Animator.SetBool("Running", true);

                
            }
            

            else
            {
                transform.Translate(0, 0, 0, Space.Self);
                _playerRigidbody.AddForce(new Vector3(0,0,0));
            }





        }
        
        else if (FinishLineTrigger.touchingFinishline == true)
        {


             transform.Translate(0, 0, 0);
            _playerRigidbody.AddForce(new Vector3(0, 0, 0));
            Animator.SetBool("Running", false);
            


        }
        

    }
}
