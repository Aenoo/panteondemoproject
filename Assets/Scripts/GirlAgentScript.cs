﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;

public class GirlAgentScript : Agent
{
    Rigidbody rBody;
    [SerializeField]
    private Transform startPositon;

    public Transform target;

    public ParticleSystem _hitParticle;

    public Rigidbody _girlRigidbody;

    public float _forceMultiplier, forwardSpeed;

    [SerializeField]
    private Animator _girlAnimController;

    private bool _girlDied;

    public static bool _botTouchFinish;

    private float _platformDistance;






    public override void Initialize()
    {
        _platformDistance = Vector3.Distance(target.position, startPositon.position);

        rBody = GetComponent<Rigidbody>();
        _girlDied = false;
        _botTouchFinish = false;
    }

    private void Update()
    {
      
        //** when the game begin, it provides to run to forwward on the z axis

        if (FixedTouchArea.firstPress && _botTouchFinish==false && FinishLineTrigger.touchingFinishline == false)
        {
            if (_girlDied == false &&_botTouchFinish == false)
            {
                transform.Translate(0, 0, forwardSpeed * Time.deltaTime, Space.Self);
                _girlAnimController.SetBool("Running", true);
            }

            else
            {
                transform.Translate(0, 0, 0, Space.Self);
                _girlRigidbody.AddForce(new Vector3(0, 0, 0));
            }


        }
       
        else if (FinishLineTrigger.touchingFinishline == true)
        {
            transform.Translate(0, 0, 0);
            _girlRigidbody.AddForce(new Vector3(0, 0, 0));
            this.transform.rotation = new Quaternion(0, 0, 0, 0);
            this._girlAnimController.SetBool("Running", false);

            


        }
        else
        {
            this.transform.rotation = new Quaternion(0,0,0,0);
        }


    }


    public override void OnEpisodeBegin()
    {
        //Reset agent
        this.rBody.angularVelocity = Vector3.zero;
        this.rBody.velocity = Vector3.zero;

        _girlAnimController.SetBool("Dying", false);

        this.transform.position = startPositon.position;
        this.transform.rotation = startPositon.rotation;

    }

    public override void CollectObservations(VectorSensor sensor)
    {

        //Target and Agent positions & Agent rotation
        sensor.AddObservation(target.localPosition);
        sensor.AddObservation(this.transform.localPosition);
        sensor.AddObservation(this.transform.rotation.y);

    }  

   
    //** 3 action here 
    public override void OnActionReceived(ActionBuffers actionBuffers)

    {


        var rotateDir = Vector3.zero;

        //**************  DiscreteActions[0] == 0 not rotating 
        //                DiscreteActions[0] == 1 positive rotating
        //                DiscreteActions[0] == -1 negative rotating *********



        if (actionBuffers.DiscreteActions[0] == 1)
        {
            
            rotateDir = transform.up * 1f;

        }
        else if (actionBuffers.DiscreteActions[0] == 2)
        {
            rotateDir = transform.up * -1f;
        }
             

       

            transform.Rotate(rotateDir, Time.deltaTime * 60f * 2f);

       



    }



   //**** give punish the bot when she the fail
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("HorizontalObstacle") || col.gameObject.CompareTag("StaticObstacle") || col.gameObject.CompareTag("UnderPlatform") || col.gameObject.CompareTag("HalfDonut"))
        {
            StartCoroutine(WaitGirlStart());
        }

        else if (col.gameObject.CompareTag("RotatorStick"))
        {


            ContactPoint contact = col.contacts[0];

            Vector3 colPos = contact.point;

            Vector3 forceDir = ((transform.position - colPos) / (colPos - transform.position).magnitude);

            forceDir = new Vector3(forceDir.x, 0.3f, forceDir.z);



            _hitParticle.Play();
            _girlRigidbody.AddForce(forceDir * _forceMultiplier, ForceMode.Force);

            //AddReward(-0.1f);
            AddReward(-Vector3.Distance(target.transform.position, transform.position) / _platformDistance);


        }

        else if (col.gameObject.CompareTag("FinishLine"))
        {
           

            SetReward(1f);

            _botTouchFinish = true;

            // *****In Training mode  EndEpisode() opening

            //   EndEpisode();
        }


    }

    //********To show dying animation by waiting

    // give punish the bot when she the fail
    IEnumerator WaitGirlStart()
    {
        _hitParticle.Play();
        _girlAnimController.SetBool("Dying", true);
        _girlDied = true;

        AddReward(-Vector3.Distance(target.transform.position, transform.position) / _platformDistance);
        //AddReward(-0.1f);
        yield return new WaitForSeconds(1);
        _girlDied = false;
        EndEpisode();
    }



    //**** To control the bot yourself
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        int turnAction = 0;
       
        if (Input.GetKey(KeyCode.A))
        {
            // turn left
            turnAction = 2;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            // turn right
            turnAction = 1;
        }

        // Put the actions into the array
        actionsOut.DiscreteActions.Array[0] = turnAction;
        
    }








}
