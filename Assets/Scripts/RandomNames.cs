﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class RandomNames : MonoBehaviour
{

    //** add this script to all racers
    [SerializeField]
    private GameObject _nameText;

    
    public string myName;


    public void Start()
    {
        
    }


    public void Update()
    {
        PickRandomFromList();
    }

    public void PickRandomFromList()
    {     
        _nameText.GetComponent<TextMeshProUGUI>().text = myName;        
        
    }
}
