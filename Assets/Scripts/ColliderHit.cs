﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderHit : MonoBehaviour
{
    [SerializeField]
    private GameManager _gameManager;   

    public Rigidbody _playerRigidbody;
    public GameObject _player;

    public float _forceMultiplier;

    public ParticleSystem _hitParticle;




    // *********** dying by hitting obstacles collison

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("HorizontalObstacle") || other.gameObject.CompareTag("StaticObstacle") || other.gameObject.CompareTag("UnderPlatform") || other.gameObject.CompareTag("HalfDonut"))
        {
            //avoiding one more collision hitting
            if (GameManager._died == false)
            {

                _hitParticle.Play();
                StartCoroutine(_gameManager.GetComponent<GameManager>().WaitBackToStart());


            }





        }

        //**** RotatorStick is applying force when collision enter

        else if (other.gameObject.CompareTag("RotatorStick"))
        {
           

            ContactPoint contact = other.contacts[0];

            Vector3 colPos = contact.point;

            Vector3 forceDir = ((transform.position - colPos) / (colPos - transform.position).magnitude);

            forceDir = new Vector3(forceDir.x, 0.3f, forceDir.z);



            _hitParticle.Play();
            _playerRigidbody.AddForce(forceDir * _forceMultiplier, ForceMode.Force);

            

          

        }



    }





}
