﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationController_Player : MonoBehaviour
{
    public float Yaxis;    
    public float RotationSensitivity = 1f;

    Vector3 targetRotation;
    Vector3 currentVelocity;

    float RotationMinX = -60f;
    float RotationMaxX = 60f;
    float smoothTime = 0.1f;

    public FixedTouchArea touchField;  

    

    void Update()
    {

        //**** Rotating the character Yaxis from FixedTouchAreaScript


        if (FinishLineTrigger.touchingFinishline == false)
        {

            Yaxis += touchField.TouchDist.x * RotationSensitivity;

            Yaxis = Mathf.Clamp(Yaxis, RotationMinX, RotationMaxX);

            targetRotation = Vector3.SmoothDamp(targetRotation, new Vector3(0, Yaxis), ref currentVelocity, smoothTime);

            transform.eulerAngles = targetRotation;


            // Reseting the rotation of the player when player died
            if (GameManager._died == true)
            {
                Yaxis = 0f;
            }
        }
    }
}
