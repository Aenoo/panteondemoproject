﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLineTrigger : MonoBehaviour
{
    public static bool touchingFinishline;

    public GameObject gameManager;

    public GameObject[] _racers;

    




    public void Start()
    {
        touchingFinishline = false;

        

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            touchingFinishline = true;



        }

        else if (other.tag == "Girl")
        {
            GirlAgentScript._botTouchFinish = true;

            GetComponent<BoxCollider>().enabled = false;

          
            {
                gameManager.GetComponent<GameManager>()._restartGameButton.SetActive(true);
                gameManager.GetComponent<GameManager>()._youLostText.SetActive(true);
              
                Destroy(gameManager.GetComponent<GameManager>()._wall);
                gameManager.GetComponent<GameManager>()._smokeParticle.Play();
                gameManager.GetComponent<GameManager>()._fireWorkParticle.Play();
            }



        }
        if (other.tag == "Player" || other.tag == "Girl")
        {
            GetComponent<BoxCollider>().enabled = false;

            for (int i = 0; i < _racers.Length; i++)
            {

                if (_racers[i].name == other.name)
                {
                    if (other.name != "Player")
                    {
                        _racers[i].GetComponent<Animator>().SetBool("Dancing", true);
                        _racers[i].transform.rotation = new Quaternion(0, 0, 0, 0);
                    }
                    else
                    {
                        _racers[i].GetComponent<Animator>().SetBool("Running", true);
                        _racers[i].transform.rotation = new Quaternion(0, 0, 0, 0);
                        gameManager.GetComponent<GameManager>()._paintTheWallText.SetActive(true);

                    }
                }
                else
                {
                    _racers[i].GetComponent<Animator>().SetBool("Crying", true);
                    _racers[i].transform.rotation = new Quaternion(0, 0, 0, 0);
                }


            }


        }

    }


}
