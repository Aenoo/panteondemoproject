﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingStick : MonoBehaviour
{
    [SerializeField]
    private float _rotateSpeed;  

   

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0,_rotateSpeed,0);
    }

   

}
