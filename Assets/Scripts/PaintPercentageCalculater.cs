﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PaintPercentageCalculater : MonoBehaviour
{


    // This class for control real time percentage of painted wall
    //Breaking the x and y axes of the wall into pieces , send the ray for the scaning of the wall

    [SerializeField]
    private Transform _topLeft, _topRight, _bottomLeft, _bottomRight;

    [SerializeField]
    private GameObject _percentageText,_restartGameButton, _congratulationsText;

    private float _wallHeight, _wallWidth;

    int counter_X;
    int counter_Y;
    [SerializeField]
    private float _divisionCount;

    private float _stepLength_X;
    private float _stepLength_Y;
    Vector3 _positionHolder;

    private float _percentage;

    private float _brushHitCount, _wallHitCount;
    
    public GameObject mainCamera,_wall;

    



    public Ray ray;
    public RaycastHit hitInfo;
    void Start()
    {        
        _percentageText.SetActive(false);

        _wallHitCount = 0;
        _brushHitCount = 0;

        counter_Y = 0;
        counter_X = 0;
        _positionHolder = _topLeft.position;
        _wallHeight = _topLeft.position.y - _bottomLeft.position.y;
        _wallWidth = _topRight.position.x - _topLeft.position.x;
        _stepLength_X = _wallWidth / _divisionCount;
        _stepLength_Y = _wallHeight / _divisionCount;

    }

    // Update is called once per frame
    void Update()
    {
        if (FinishLineTrigger.touchingFinishline == true && GameManager._finished == false)
        {
            _percentageText.SetActive(true);



            // sending ray from camera to scan x axis
            if (counter_X <= _divisionCount)
            {               
                ray = new Ray(mainCamera.transform.position, (_positionHolder + new Vector3(1, 0, 0) * _stepLength_X * counter_X) - mainCamera.transform.position);

                counter_X++;
                Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow, 2f);

            }
            else
            {

                counter_X = 0;
                counter_Y++;
                _positionHolder = _topLeft.position + new Vector3(0, -1, 0) * _stepLength_Y * counter_Y;

                // sending ray from camera to scan Y axis
                if (_divisionCount < counter_Y)
                {
                    _positionHolder = _topLeft.position;
                    counter_Y = 0;
                    counter_X = 0;

                    _percentage = _brushHitCount / (_wallHitCount + _brushHitCount);

                    


                    _percentageText.GetComponent<TextMeshProUGUI>().text = "%" + (int)(100 * _percentage);


                    // becasue if we don't give 0 here , it goes on to infinity
                    _wallHitCount = 0;
                    _brushHitCount = 0;

                    if ((int)_percentage == 1)
                    {
                        GameManager._finished = true;            

                    }

                }

            }



            //To Find how many wall space and brush space,send ray to wall and brush

            if (Physics.Raycast(ray, out hitInfo))
            {

                
                if (hitInfo.transform.tag == "Wall")
                {
                    _wallHitCount++;
                }
                else if (hitInfo.transform.tag == "Brush")
                {                   

                    _brushHitCount++;
                }

            }
        }
    }
   
}
