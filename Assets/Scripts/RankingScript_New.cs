﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class RankingScript_New : MonoBehaviour
{


    public GameObject[] botList;
    public GameObject[] ringList;
    public GameObject[] _boardNames;



    public List<GameObject> characterList = new List<GameObject>();

    private List<float> distanceList = new List<float>();
    public List<int> rankList = new List<int>();
    private List<int> tempRankList = new List<int>();

    int ondeKacKisiVar;





    GameObject character;
    float distance;


    void Awake()
    {
        //createRingList();
        //createChracterList();
        // createBoardNamesList();

        _boardNames[0].GetComponent<TextMeshProUGUI>().color = Color.black;
        _boardNames[0].GetComponent<TextMeshProUGUI>().fontSize = 55;


    }

    // Update is called once per frame
    void Update()
    {

        createDistanceList();

        // print here
        for (int i = 0; i < characterList.Count; i++)
        {
            _boardNames[i].GetComponent<RankTextPosition>().myRank = rankList[i];

            _boardNames[i].GetComponent<TextMeshProUGUI>().text = +rankList[i] + "." + characterList[i].GetComponent<RandomNames>().myName;
        }


        FindFirstRacer();

    }



    // find first racer to show the ring 
    private void FindFirstRacer()
    {
        for (int i = 0; i < characterList.Count; i++)
        {
            if (rankList[i] == 1)
            {

                ringList[i].SetActive(true);
            }
            else
                ringList[i].SetActive(false);

        }
    }




    private void createDistanceList()
    {
        distanceList.Clear();
        tempRankList.Clear();

        //** Distance list include of racers position of z axis

        for (int i = 0; i < characterList.Count; i++)
        {
            character = characterList[i];
            distance = character.transform.position.z;
            distanceList.Add(distance);
        }

        //**** Find here how many racers are in front of you

        for (int j = 0; j < characterList.Count; j++)
        {
            ondeKacKisiVar = 0;

            for (int i = 0; i < characterList.Count; i++)
            {

            

                if (characterList[j].transform.position.z < characterList[i].transform.position.z)
                {

                    if (i != j)
                    {
                        ondeKacKisiVar++;
                    }

                }

            }
            // create temporary list to add rank list

            tempRankList.Add(ondeKacKisiVar + 1);


        }

        rankList = tempRankList;








    }




    /*
    // **********  add all bot and player to the characterlist ********
    private void createChracterList()
    {
        characterList.Add(GameObject.FindGameObjectWithTag("Player"));
        botList = GameObject.FindGameObjectsWithTag("Girl");
        for (int i = 0; i < botList.Length; i++)
        {
            characterList.Add(botList[i]);
        }



    }


    // **********  add all rings to RingList
    private void createRingList()
    {
        ringList = GameObject.FindGameObjectsWithTag("Ring");
    }



    // **********  add all text about name  to the boardNamesList 
    private void createBoardNamesList()
    {
        _boardNames = GameObject.FindGameObjectsWithTag("RankText");
    }
*/


}







