﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paintable : MonoBehaviour
{
    //****** creating paint in this class


    [SerializeField]
    private GameObject _brush;

    [SerializeField]
    private Transform _topLeftBox, _bottomRightBox;

    public float BrushSize = 0.1f;


    Ray ray;
    RaycastHit hitInfo;

   // private Vector3 previousBrushSpawnPoint; //to prevent spawning too many brushes at the same point




    // Update is called once per frame
    void FixedUpdate()
    {


        // create brush according ray of camera

        if (Input.GetMouseButton(0) && FinishLineTrigger.touchingFinishline == true && GameManager._finished == false)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            Debug.DrawRay(ray.origin, ray.direction * 10, Color.red, 2f);

            if (Physics.Raycast(ray, out hitInfo))
            {

                //****** preventing of painting outside the wall                
                if (hitInfo.transform.position.x >= _topLeftBox.position.x && hitInfo.transform.position.x <= _bottomRightBox.position.x
                    && hitInfo.transform.position.y <= _topLeftBox.position.y && hitInfo.transform.position.y >= _bottomRightBox.position.y)
                {

                    if (hitInfo.transform.tag == "Wall")
                    {
                        var go = Instantiate(_brush, hitInfo.point + new Vector3(0, 0, -0.01f), Quaternion.Euler(-90, 0, 0), transform);
                        go.transform.localScale = Vector3.one * BrushSize;
                    }
                    if (hitInfo.transform.tag == "Brush")
                    {
                        var go = Instantiate(_brush, hitInfo.point + new Vector3(0, 0, 0), Quaternion.Euler(-90, 0, 0), transform);
                        go.transform.localScale = Vector3.one * BrushSize;
                    }




                }
            }

        }


    }
}












