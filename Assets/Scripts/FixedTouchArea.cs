﻿using UnityEngine;
using UnityEngine.EventSystems;

public class FixedTouchArea : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{


    //******************************  
    // Attach the Image on canvas  and create the touchable area with this script


    [HideInInspector]
    public Vector2 TouchDist;
    [HideInInspector]
    public Vector2 PointerOld;
    [HideInInspector]
    protected int PointerId;
    [HideInInspector]
    public bool Pressed;

    public static bool firstPress;
    

    // Use this for initialization
    void Start()
    {
        
        firstPress = false;
    
        
        
    }

    void Update()
    {
        
        if (Pressed)
        {

            firstPress = true;
            if (PointerId >= 0 && PointerId < Input.touches.Length)
            {
                TouchDist = Input.touches[PointerId].position - PointerOld;
                PointerOld = Input.touches[PointerId].position;
                
            }
            else
            {
                TouchDist = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - PointerOld;
                PointerOld = Input.mousePosition;
                

            }
        }
        else
        {
            TouchDist = new Vector2();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Pressed = true;
        PointerId = eventData.pointerId;
        PointerOld = eventData.position;
       

    }


    public void OnPointerUp(PointerEventData eventData)
    {
        Pressed = false;
      
    }

}