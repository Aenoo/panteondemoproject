﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingPlatform : MonoBehaviour
{
    [SerializeField]
    private GameObject _player,_girl;

    [SerializeField]
    private float _rotateSpeed, _slidingSpeed;



    void Update()
    {
        transform.Rotate(0, 0, _rotateSpeed);
    }


    // when collison stay with player and bots apply force in oppisite direction of rotating of platform
    void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {

            foreach (ContactPoint contact in other.contacts)
            {
                _player.transform.Translate(new Vector3(0.1f, 0, 0) * Time.deltaTime * _slidingSpeed);
            }
        }
        else if (other.gameObject.tag == "Girl")
        {
            foreach (ContactPoint contact in other.contacts)
            {
                _girl.transform.Translate(new Vector3(0.1f, 0, 0) * Time.deltaTime * _slidingSpeed);
            }
        }


    }
}
