﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform Playertransform;
    private Vector3 _cameraOffset;
    public Transform finishView;
    public Transform runnerView;

    [Range(0.1f, 1.0f)]
    public float SmootActor = 1f;

    // Start is called before the first frame update
    void Start()
    {
        _cameraOffset = transform.position - Playertransform.position;

    }

    // Update is called once per frame
    void LateUpdate()
    {
       
       
        //****  on click start button ,switch the runner view mode  
        if (GameManager._gameStarted == false)
        {
            
            transform.rotation = Quaternion.Slerp(transform.rotation, runnerView.rotation,0.1f);
            transform.position = runnerView.transform.position;
            _cameraOffset = transform.position - Playertransform.position;


        }
        else
        {
           
            // ***********  tap to start
            if (FinishLineTrigger.touchingFinishline == false && GameManager._gameStarted == true && transform)
            {

                Vector3 newPos = Playertransform.position + _cameraOffset;

                transform.position = Vector3.Slerp(transform.position, newPos, SmootActor);
            }

            else if (FinishLineTrigger.touchingFinishline == true)
            {
                
                SmootActor = 0.2f;
                transform.position = Vector3.Slerp(transform.position, finishView.position, 0.1f);
                transform.rotation = Quaternion.Slerp(transform.rotation, finishView.rotation, 0.1f);
            }

        }
    }
}
