﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalMovemetObstacles : MonoBehaviour
{
    [SerializeField]
    private float _changeDirectionDelay, _speed;
    [SerializeField]
    private Transform _startPoint, _endPoint;
    

    private Transform destinationTarget, departTarget;

    private float startTime;

    private float journeyLenght;

    bool isWaiting;

    void Start()
    {
        departTarget = _startPoint;
        destinationTarget = _endPoint;

        startTime = Time.time;
        journeyLenght = Vector3.Distance(departTarget.position, destinationTarget.position);
    }


    void Update()
    {
        Move();

    }
    // Move Horizontal obstacle from start point to end point 
    private void Move()
    {
        if (!isWaiting)
        {
            if (Vector3.Distance(transform.position, destinationTarget.position) > 0.01f)
            {
                float distCovered= (Time.time - startTime) * _speed;
                float fractionOfJourney = distCovered / journeyLenght;

                transform.position = Vector3.Lerp(departTarget.position, destinationTarget.position, fractionOfJourney);
            }
            else
            {
                isWaiting = true;
                StartCoroutine(changeDelay());
            }
        }
        
        
    }

    // When it reach to the end point, it switches the direction of movememnt 
    void ChangeDestination()
    {
        if (destinationTarget == _startPoint && departTarget == _endPoint)
        {
            destinationTarget = _endPoint;
            departTarget = _startPoint;
        }
        else
        {
            departTarget = _endPoint;
            destinationTarget = _startPoint;
        }
    }


    // Give delay on the positions
    IEnumerator changeDelay()
    {
        yield return new WaitForSeconds(_changeDirectionDelay);
        ChangeDestination();
        startTime = Time.time;
        journeyLenght = Vector3.Distance(departTarget.position, destinationTarget.position);
        isWaiting = false;
    }
}
