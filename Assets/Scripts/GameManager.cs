﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;



public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _racerList;
    public GameObject[] _starPositionList;

    public int _ranndomNumber;



    List<string> _botNames = new List<string>();


    [SerializeField]
    public GameObject _player, _tapToStartText, _restartGameButton, _congratulationsText, _wall, _playerInputPanel, _namesPanel,_youLostText,_paintTheWallText;

    [SerializeField]
    public ParticleSystem _smokeParticle, _fireWorkParticle;

    [SerializeField]
    private float _pushForce;

    [SerializeField]
    private Rigidbody _playerRigidbody;

    [SerializeField]
    private Animator _playerAnimController;

    [SerializeField]
    private Material _wallMat;

    public static bool _died = false;
    public static bool _finished = false;

    public static bool _gameStarted = true;

    bool _doOnce;


    List<string> _selectedBotNames = new List<string>();


    private void Awake()
    {

        _ranndomNumber = Random.Range(0, 5);

        _player.transform.position = _starPositionList[_ranndomNumber].transform.position;
      
  

    }
    void Start()
    {
        Application.targetFrameRate = 60;

        _botNames = new List<string> { "Frodo","Sam", "Legolas", "Aragorn", "Gandalf", "Bilbo", "Elrond", "Arwen", "Faramir", "Gimli", "Boramir", "Galadriel", "Pippin", "Merry", "Sauron", "Saruman", "Smeagol", "Radagast" };
             
        
     

        SetBotNames();

               

        _finished = false;
        _doOnce = true;
        _wallMat.color = Color.blue;
        _congratulationsText.SetActive(false);
        _congratulationsText.SetActive(false);
        _restartGameButton.SetActive(false);
        _tapToStartText.SetActive(false);
        _paintTheWallText.SetActive(false);



        _namesPanel.SetActive(false);


        //******** After the restart button, close playerInputPanel**********
        if (_gameStarted == false)
        {
            _playerInputPanel.SetActive(false);
            _namesPanel.SetActive(true);
            _tapToStartText.SetActive(true);
        }


    }

    // Update is called once per frame
    void Update()
    {
        HideStartText();
        FinishTheGame();


    }


    //**** Give a random names from _botNames list
    public void SetBotNames()
    {
        string _randomName;


        while (_selectedBotNames.Count < _racerList.Length)
        {

            _randomName = _botNames[Random.Range(0, _botNames.Count)];

            if (!_selectedBotNames.Contains(_randomName))
            {

                
                _racerList[_selectedBotNames.Count].GetComponent<RandomNames>().myName = _randomName;



                _selectedBotNames.Add(_randomName);


            }

        }

    }
   
    
   ///** define with StartButton in editor
    
    public void OnClickStartGameButton()
    {
        _playerInputPanel.SetActive(false);
        _namesPanel.SetActive(true);
        _tapToStartText.SetActive(true);


        _gameStarted = false;
        





    }

    // Hide to tap to start text and start the game
    private void HideStartText()
    {
        if (FixedTouchArea.firstPress == true && _tapToStartText.activeSelf == true)
        {
            _tapToStartText.SetActive(false);
            _gameStarted = true;


        }
    }

    public void RestartTheGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        _congratulationsText.SetActive(false);
        _gameStarted = false;

    }


    public void FinishTheGame()
    {
        if (_finished == true)
        {
            if (_doOnce == true)
            {
                _paintTheWallText.SetActive(false);
                _restartGameButton.SetActive(true);
                _congratulationsText.SetActive(true);               
                _wallMat.color = Color.red;
                _playerAnimController.SetBool("Dancing", true);
                _doOnce = false;
                Destroy(_wall);
                _smokeParticle.Play();
                _fireWorkParticle.Play();
            }



        }
    }

    //****** Returnt the player to startPositon
    public void PlayerFailingInGame()
    {
        _playerAnimController.SetBool("Dying", false);
        _died = false;
        _player.transform.position = _starPositionList[_ranndomNumber].transform.position;
        _player.transform.rotation = _starPositionList[_ranndomNumber].transform.rotation;

    }






    // To show dying animation by waiting
    public IEnumerator WaitBackToStart()
    {
        _playerAnimController.SetBool("Dying", true);
        _died = true;
        yield return new WaitForSeconds(1);
        PlayerFailingInGame();
    }










}
